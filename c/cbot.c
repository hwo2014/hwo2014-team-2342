#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"


/*Tee mutkien sis��ntulonopeus taulukko, aluarvo voidaan laskea => mutkan lyhin s�de / mutkan kokokulma (esim 180 astetta) -> t�lle voi antaa arvon 7.5
 * jos auto ajaa ulos mutkan nopeutta tiputetaan 0.5 pyk�l��, jos maksimi kulma j�� alle 53 astetta kasvatetaan vauhtia 0.2 pyk�l��*/

#define PRINT_LOCATION_UPDATE 1

#define LOCATION_RINGBUFFER_SIZE 20
#define NOT_VALID 30000
#define PI 3.14159
#define CIRCLE_DEG 360

typedef enum {STRAIGTH, CURVE} piece_shape_t ;

typedef struct{
  piece_shape_t shape;
  double length;
  double radius;
  double angle;
  double traction;
  int switchable;
}track_piece_t;

typedef struct{
  int lane_ind;
  double distance_from_cntr;
}lane_info_t;

typedef struct{
  int updated_at_lap;
  int piece_ind;
  double target_speed;
  double max_angle;
}input_speed_t;

typedef struct{
  int piece_cnt;
  track_piece_t* track_pieces;
  int lane_cnt;
  lane_info_t* lanes;
  int target_cnt;
  input_speed_t* target_speed;
}race_track_t;

typedef struct{
  int available;
  int enabled_at_tick;
  double turbo_factor;
  int duration;
  int active;
}turbo_available_t;

typedef struct{
  char* name;
}my_car_info_t;

typedef struct{
  int last_checked_piece;
}lane_switch_info_t;

typedef struct{
  int valid; //0 = not valid, !0 = valid
  int game_tick;
  int start_lane_index;
  int end_lane_index;
  int lap;
  int piece_ind;
  double in_piece_dist;
  double angle;
  double speed;
  double acceleration;
  double angle_acceleration;
  double throttle;
}current_location_t;



typedef struct{
	int ind;
	current_location_t buff[LOCATION_RINGBUFFER_SIZE];
}location_info_t;

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

int store_race_track_lanes(cJSON* lanes);
int store_race_track_pieces(cJSON* track_pieces);
int store_car_info(cJSON* car_info);
int store_turbo_available(cJSON* turbo_info);
int update_location_info(cJSON* location_info);
double calculate_throttle();
int calculate_speedometer_info();
double do_location_update_and_calculte_new_throttle(cJSON* json);
void update_ring_buff_ind();
int get_ring_buff_ind(int n);
static cJSON* switch_lane_msg();
cJSON* enable_turbo_msg();

char test_message[] = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"indianapolis\",\"name\":\"Indianapolis\",\"pieces\":[{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200,\"angle\":22.5}],\"lanes\":[{\"distanceFromCenter\":-20,\"index\":0},{\"distanceFromCenter\":0,\"index\":1},{\"distanceFromCenter\":20,\"index\":2}],\"startingPoint\":{\"position\":{\"x\":-340.0,\"y\":-96.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":30000,\"quickRace\":true}}}}";
char test_message_my_car[] = "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"Schumacher\",\"color\":\"red\"}}";
char test_message_positions[] = "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"angle\":12.0,\"piecePosition\":{\"pieceIndex\":2,\"inPieceDistance\":45.0,\"lane\":{\"startLaneIndex\":6,\"endLaneIndex\":7},\"lap\":0}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"angle\":45.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":20.0,\"lane\":{\"startLaneIndex\":1,\"endLaneIndex\":1},\"lap\":0}}],\"gameId\":\"OIUHGERJWEOI\",\"gameTick\":0}";

#if 0
char test_car_name[] = "Rosberg";
#else
char test_car_name[] = "Schumacher";
#endif

race_track_t race_track = {0,NULL,0,NULL,0,NULL};
my_car_info_t my_car = {NULL};
location_info_t location_hist;
turbo_available_t turbo_available = {0,0,0,0,0};
lane_switch_info_t lane_switch_info = {NOT_VALID};
static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}


cJSON* find_item(const char* find_string, cJSON* check){
  cJSON* found = NULL;

  if(check == NULL){ return NULL; }

  if(check->string != NULL){
    if(!strcmp(find_string, check->string)){
      return check;
    }
  }

  /*Check next if available*/
  if(check->next != NULL){
    found = find_item(find_string, check->next);
  }

  /*Check child if available*/
  if(check->child != NULL && found == NULL){
    found = find_item(find_string, check->child);
  }

  return found;
}

int store_race_track(cJSON* track){
  /* Give "track" object as an input for this function */
  int status = 0;
  status = store_race_track_pieces(find_item("pieces", track));
  status |= store_race_track_lanes(find_item("lanes", track));



#if PRINT_LOCATION_UPDATE
  int i;
  for(i = 0; i < race_track.piece_cnt; i++){
	  printf("pala %d shape %d length %f radius %f angle %f  traction %f switch %d \n",
			  i,
			  race_track.track_pieces[i].shape,
			  race_track.track_pieces[i].length,
			  race_track.track_pieces[i].radius,
			  race_track.track_pieces[i].angle,
			  race_track.track_pieces[i].traction,
			  race_track.track_pieces[i].switchable);
  }

  for(i = 0; i < race_track.lane_cnt; i++){
	  printf("rata %d distance from center %f lane_ind %d \n",
			  i,
			  race_track.lanes[i].distance_from_cntr,
			  race_track.lanes[i].lane_ind);
  }

  for(i = 0; i < race_track.target_cnt; i++){
    printf("target speed for curve %d piece_ind %d target_speed %f\n",
        i,
        race_track.target_speed[i].piece_ind,
        race_track.target_speed[i].target_speed);
  }

#endif

  return status;
}

int store_race_track_pieces(cJSON* track_pieces){

#define MAX_TARGET_SPEED 0.07
  /* Give "pieces" object as an input for this function */

  cJSON *track_piece_info = NULL;
  int i, target_speed_cnt, curve_direction, starting_piece;
  double total_angle, min_radius, target_speed;

  if(track_pieces == NULL){
    return 0;
  }


  starting_piece = 0;
  target_speed_cnt = 0;
  curve_direction = 0;
  total_angle = 0;
  min_radius = 0;
  //allocate & clear race_track
  race_track.piece_cnt = cJSON_GetArraySize(track_pieces);
  race_track.track_pieces = malloc(race_track.piece_cnt * sizeof(track_piece_t));
  memset(race_track.track_pieces,0,race_track.piece_cnt * sizeof(track_piece_t));
  race_track.target_speed = malloc(race_track.piece_cnt*sizeof(input_speed_t));
  memset(race_track.target_speed,0,race_track.piece_cnt*sizeof(input_speed_t));

  for(i = 0; i < race_track.piece_cnt; i++){

    track_piece_info =  (cJSON_GetArrayItem(track_pieces, i))->child;

    while(track_piece_info){

      if(track_piece_info->string){
        if( !strcmp("length", track_piece_info->string)){
          race_track.track_pieces[i].length = track_piece_info->valuedouble;
          race_track.track_pieces[i].shape = STRAIGTH;
        } else if( !strcmp("radius", track_piece_info->string)) {
          race_track.track_pieces[i].radius = track_piece_info->valuedouble;
          race_track.track_pieces[i].shape = CURVE;
        } else if( !strcmp("angle", track_piece_info->string)) {
          race_track.track_pieces[i].angle = track_piece_info->valuedouble;
          race_track.track_pieces[i].shape = CURVE;
        } else if( !strcmp("switch", track_piece_info->string)) {
          race_track.track_pieces[i].switchable = track_piece_info->valueint;
        }
      }

      track_piece_info = track_piece_info->next;
    }

         /*store target speed info*/
         if(race_track.track_pieces[i].shape == CURVE && race_track.track_pieces[i].angle > 0){

           if(curve_direction != 1){

             //store information of previous target speed
             if (total_angle != 0){
               target_speed_cnt++;

               if(total_angle < 0){
                 total_angle *= -1;
               }
               target_speed = MAX_TARGET_SPEED * min_radius;

               if(target_speed > 30){
                 target_speed = 30;
               }
               race_track.target_speed[target_speed_cnt-1].target_speed = target_speed;
               race_track.target_speed[target_speed_cnt-1].piece_ind = starting_piece;
             }

             //start collection information for next target speed
             starting_piece = i;
             total_angle = race_track.track_pieces[i].angle;
             min_radius = race_track.track_pieces[i].radius;
           } else{
             total_angle += race_track.track_pieces[i].angle;
             min_radius = min_radius < race_track.track_pieces[i].radius ? min_radius : race_track.track_pieces[i].radius;
           }

           curve_direction = 1;

         } else if (race_track.track_pieces[i].shape == CURVE && race_track.track_pieces[i].angle < 0){
           if(curve_direction != -1){
             //store information of previous target speed
             if (total_angle != 0){
               target_speed_cnt++;

               if(total_angle < 0){
                 total_angle *= -1;
               }
               target_speed = MAX_TARGET_SPEED * min_radius;

               if(target_speed > 30){
                 target_speed = 30;
               }
               race_track.target_speed[target_speed_cnt-1].target_speed = target_speed;
               race_track.target_speed[target_speed_cnt-1].piece_ind = starting_piece;
             }

             //start collection information for next target speed
             starting_piece = i;
             total_angle = race_track.track_pieces[i].angle;
             min_radius = race_track.track_pieces[i].radius;
           } else{
             total_angle += race_track.track_pieces[i].angle;
             min_radius = min_radius < race_track.track_pieces[i].radius ? min_radius : race_track.track_pieces[i].radius;
           }

           curve_direction = -1;

         } else {
           if(curve_direction != 0 && total_angle != 0){
             //store information of previous target speed
             target_speed_cnt++;

             if(total_angle < 0){
               total_angle *= -1;
             }
             target_speed = MAX_TARGET_SPEED * min_radius;
             if(target_speed > 30){
               target_speed = 30;
             }
             race_track.target_speed[target_speed_cnt-1].target_speed = target_speed;
             race_track.target_speed[target_speed_cnt-1].piece_ind = starting_piece;
           }

           starting_piece = 0;
           curve_direction = 0;
           total_angle = 0;
           min_radius = 0;
         }
  }

  race_track.target_cnt = target_speed_cnt;

  return 1;
}

int store_race_track_lanes(cJSON* lanes){

  /* Give "lanes" object as an input for this function */

  cJSON* lane_info = NULL;
  int i;
  if(lanes == NULL){
    return 0;
  }

  //allocate & clear race_track
  race_track.lane_cnt = cJSON_GetArraySize(lanes);
  race_track.lanes = malloc(race_track.lane_cnt * sizeof(lane_info_t));
  memset(race_track.lanes,0,race_track.lane_cnt * sizeof(lane_info_t));

  for(i = 0; i < race_track.lane_cnt; i++){

    lane_info =  (cJSON_GetArrayItem(lanes, i))->child;

    while(lane_info){
      if(lane_info->string){
        if( !strcmp("distanceFromCenter", lane_info->string)){
          race_track.lanes[i].distance_from_cntr = lane_info->valuedouble;
        } else if( !strcmp("index", lane_info->string)) {
          race_track.lanes[i].lane_ind = lane_info->valueint;
        }
      }
      lane_info = lane_info->next;
    }
  }

  return 1;
}

int store_car_info(cJSON* car_info){
  /* Give "data" from yourCar message object as an input for this function */
  cJSON* car_name;

  if(car_info == NULL){
    printf("car_info on NULL\n");
    return 0;
  }

  car_name = find_item("name", car_info);

  if(car_name != NULL && car_name->valuestring != NULL){
    my_car.name = malloc(strlen(car_name->valuestring)+1);
    strcpy(my_car.name, car_name->valuestring);
  }

  return 1;
}

int update_location_info(cJSON* location_info){

  /* Give "data" from carPositions message object as an input for this function */
  cJSON* info = NULL;
  cJSON* current_info_obj = NULL;
  cJSON* my_car_info_obj = NULL;

  if(location_info == NULL){
    printf("location_info on NULL\n");
    return 0;
  }

  /* find my car's information */
  current_info_obj = (location_info->child);

  while(current_info_obj){
    info = (current_info_obj->child);

    info = find_item("name", info);
    if (info && info->type == cJSON_String && info->valuestring && !strcmp(my_car.name, info->valuestring)){
      my_car_info_obj = current_info_obj->child;
      break;
    } else {
      current_info_obj = current_info_obj->next;
    }
  }

  if(current_info_obj == NULL){
    return 0;
  }

  memset(location_hist.buff+location_hist.ind,0,sizeof(current_location_t));
  location_hist.buff[location_hist.ind].acceleration = NOT_VALID;
  location_hist.buff[location_hist.ind].angle_acceleration = NOT_VALID;
  location_hist.buff[location_hist.ind].speed = NOT_VALID;

  location_hist.buff[location_hist.ind].valid = 1;
  /* Find and set angle */
  info = find_item("angle", my_car_info_obj);
  if (info != NULL && info->type == cJSON_Number){
 	 location_hist.buff[location_hist.ind].angle = info->valuedouble;
  }

  /* Find and set pieceIndex*/
  info = find_item("pieceIndex", my_car_info_obj);
  if (info != NULL && info->type == cJSON_Number){
 	 location_hist.buff[location_hist.ind].piece_ind = info->valueint;
  }

  /* Find and set inPieceDistance*/
  info = find_item("inPieceDistance", my_car_info_obj);
  if (info != NULL && info->type == cJSON_Number){
 	 location_hist.buff[location_hist.ind].in_piece_dist = info->valuedouble;
  }

  /* Find and set startLaneIndex*/
  info = find_item("startLaneIndex", my_car_info_obj);
  if (info != NULL && info->type == cJSON_Number){
 	 location_hist.buff[location_hist.ind].start_lane_index = info->valueint;
  }

  /* Find and set endLaneIndex*/
  info = find_item("endLaneIndex", my_car_info_obj);
  if (info != NULL && info->type == cJSON_Number){
 	 location_hist.buff[location_hist.ind].end_lane_index = info->valueint;
  }

  /* Find and set lap*/
  info = find_item("lap", my_car_info_obj);
  if (info != NULL && info->type == cJSON_Number){
 	 location_hist.buff[location_hist.ind].lap = info->valueint;
  }

  /* Find and set gameTick*/
  info = find_item("gameTick", location_info);
  if (info != NULL && info->type == cJSON_Number){
 	 location_hist.buff[location_hist.ind].game_tick = info->valueint;
  } else {
	  location_hist.buff[location_hist.ind].game_tick = -1;
  }

  return 1;
}

int store_turbo_available(cJSON* turbo_info){

  cJSON* info = NULL;

  //printf("store_turbo_available\n");

  if(turbo_info == NULL){
    return 0;
  }

  info = find_item("turboDurationTicks", turbo_info);
  if (info != NULL && info->type == cJSON_Number){
    turbo_available.duration = info->valueint;
  } else {
    turbo_available.duration = NOT_VALID;
  }

  info = find_item("turboFactor", turbo_info);
  if (info != NULL && info->type == cJSON_Number){
    turbo_available.turbo_factor = info->valuedouble;
  } else {
    turbo_available.turbo_factor = NOT_VALID;
  }

  turbo_available.available = 1;

  //printf("turbo_factor %f duration %d available %d\n", turbo_available.turbo_factor, turbo_available.duration, turbo_available.available );

  return 1;
}


void update_ring_buff_ind(){

  location_hist.ind++;
  if(location_hist.ind == LOCATION_RINGBUFFER_SIZE){
    location_hist.ind = 0;
  }

  return ;
}

int get_ring_buff_ind(int n){

  /* (-1 * LOCATION_RINGBUFFER_SIZE) <= n <= 0*/

  int tmp_ind = 0;
  tmp_ind = location_hist.ind + n;
  if(tmp_ind < 0){
    tmp_ind = LOCATION_RINGBUFFER_SIZE + tmp_ind;
  }

  return tmp_ind;
}

int calculate_speedometer_info(){

  int buff_ind_n = get_ring_buff_ind(0);
  int buff_ind_n_1 = get_ring_buff_ind(-1);
  int i;
  double radius, angle, lane_delta, length, in_piece_dist_n, in_piece_dist_n_1 ;

  if(!location_hist.buff[buff_ind_n].valid){
    printf("location info not available in calculate_speedometer_info for ind N\n");
    return 0;
  }

  if(!location_hist.buff[buff_ind_n_1].valid){
    printf("location info not available in calculate_speedometer_info for ind N-1\n");
    return 0;
  }

  /*Calculate current speed*/
  if(location_hist.buff[buff_ind_n].piece_ind == location_hist.buff[buff_ind_n_1].piece_ind){

	  //measurements are from same piece
    in_piece_dist_n = location_hist.buff[buff_ind_n].in_piece_dist;
    in_piece_dist_n_1 = location_hist.buff[buff_ind_n_1].in_piece_dist;

    location_hist.buff[buff_ind_n].speed = in_piece_dist_n - in_piece_dist_n_1;

  } else if ((location_hist.buff[buff_ind_n].piece_ind != location_hist.buff[buff_ind_n_1].piece_ind) &&
		  race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind].shape == STRAIGTH){
    // measurements are from separate pieces, previous part was straigth line

    in_piece_dist_n = location_hist.buff[buff_ind_n].in_piece_dist;
    in_piece_dist_n_1 = location_hist.buff[buff_ind_n_1].in_piece_dist;
    length = race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind].length;


    location_hist.buff[buff_ind_n].speed = in_piece_dist_n + (length - in_piece_dist_n_1);

  } else if ((location_hist.buff[buff_ind_n].piece_ind != location_hist.buff[buff_ind_n_1].piece_ind) &&
      race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind].shape == CURVE){
    // measurements are from separate pieces, previous part was curve

    in_piece_dist_n = location_hist.buff[buff_ind_n].in_piece_dist;
    in_piece_dist_n_1 = location_hist.buff[buff_ind_n_1].in_piece_dist;

    radius = race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind].radius;
    angle = race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind].angle;

    lane_delta = 0;
    for(i = 0; i < race_track.lane_cnt; i++){
      if(location_hist.buff[buff_ind_n_1].start_lane_index == race_track.lanes[i].lane_ind){
        lane_delta = race_track.lanes[i].distance_from_cntr;
        break;
      }
    }

    if (angle >= 0){
      lane_delta = lane_delta * -1;
    } else {
    	angle = angle*-1;
    }

    location_hist.buff[buff_ind_n].speed = in_piece_dist_n + (((2*PI*(radius + lane_delta)*angle)/CIRCLE_DEG) - in_piece_dist_n_1);

  }

  /*Calculate current acceleration*/
  if (location_hist.buff[buff_ind_n_1].speed != NOT_VALID){
    location_hist.buff[buff_ind_n].acceleration = location_hist.buff[buff_ind_n].speed - location_hist.buff[buff_ind_n_1].speed;
  } else {
    location_hist.buff[buff_ind_n].acceleration = NOT_VALID;
  }

  if (location_hist.buff[buff_ind_n_1].angle != NOT_VALID){
    location_hist.buff[buff_ind_n].angle_acceleration = location_hist.buff[buff_ind_n].angle - location_hist.buff[buff_ind_n_1].angle;
  } else {
    location_hist.buff[buff_ind_n].angle_acceleration = NOT_VALID;
  }


	return 1;
}

double calculate_throttle(){

#define TICS_BEFORE_BREAK 8
#define TICS_BEFORE_THROTTLE 5
#define MAX_INCOMING_ANGLE 20
#define MAX_INCOMING_SPEED 7.5
#define ANGLE_DELTA_PROS 0.045
#define ANGLE_LIMIT 60
#define DEFAULT_BREAKING_ACC -0.07

  double throttle = 0.725;

	int buff_ind_n = get_ring_buff_ind(0);
	int buff_ind_n_1 = get_ring_buff_ind(-1);
	int i;
	double dist_to_event,ticks_to_event;
	double piece_length, in_piece_dist, speed, acceleration,angle_acceleration;
	double radius, curve_angle, lane_delta, angle_delta, target_speed;
	double abs_car_angle, current_angle, tmp;
	piece_shape_t shape_current;
	track_piece_t* piece;

	shape_current = race_track.track_pieces[location_hist.buff[buff_ind_n].piece_ind].shape;

	in_piece_dist = location_hist.buff[buff_ind_n].in_piece_dist;

	current_angle = location_hist.buff[buff_ind_n].angle;

	speed = location_hist.buff[buff_ind_n].speed;
	if(speed == NOT_VALID){
	  speed = 0;
	}

	acceleration = location_hist.buff[buff_ind_n].acceleration;
	if(acceleration == NOT_VALID){
	  acceleration = 0;
	}

	angle_acceleration = location_hist.buff[buff_ind_n].angle_acceleration;
	if(acceleration == NOT_VALID){
		angle_acceleration = 0;
	}

  abs_car_angle = location_hist.buff[buff_ind_n].angle;
  if (abs_car_angle < 0 ){
    abs_car_angle *= -1;
  }

  piece_length = race_track.track_pieces[location_hist.buff[buff_ind_n].piece_ind].length;


  if(shape_current == STRAIGTH){

    //calculate how long it takes to curve if throttle is released now
	  dist_to_event = piece_length - in_piece_dist;
	  i = 1;
	  while (i < race_track.piece_cnt){

	      if(location_hist.buff[buff_ind_n].piece_ind + i >= race_track.piece_cnt){
	    	piece = &race_track.track_pieces[(location_hist.buff[buff_ind_n].piece_ind + i)-race_track.piece_cnt];
	      } else {
	        piece = &race_track.track_pieces[location_hist.buff[buff_ind_n].piece_ind + i];
	      }

	      if(piece->shape == STRAIGTH){
	    	  dist_to_event += piece->length;
	      } else {
	    	  break;
	      }
		  i++;
	  }

	  /*check target speed*/
	  target_speed = 30;
    i = 0;
    while (i < race_track.target_cnt){

        if(location_hist.buff[buff_ind_n].piece_ind <= race_track.target_speed[i].piece_ind){
          if(race_track.target_speed[i].max_angle < abs_car_angle){
            race_track.target_speed[i].max_angle = abs_car_angle;
          }

          if(location_hist.buff[buff_ind_n].lap != race_track.target_speed[i].updated_at_lap && race_track.target_speed[i].max_angle < 20){
            race_track.target_speed[i].updated_at_lap = location_hist.buff[buff_ind_n].lap;
            race_track.target_speed[i].target_speed += 0.4;
            printf("curve %d Max angle was %f increase target speed with 0.4\n",i,race_track.target_speed[i].max_angle);
          } else if(location_hist.buff[buff_ind_n].lap != race_track.target_speed[i].updated_at_lap && race_track.target_speed[i].max_angle < 35){
            race_track.target_speed[i].updated_at_lap = location_hist.buff[buff_ind_n].lap;
            race_track.target_speed[i].target_speed += 0.2;
            printf("curve %d Max angle was %f increase target speed with 0.2\n",i,race_track.target_speed[i].max_angle);
          } else if(location_hist.buff[buff_ind_n].lap != race_track.target_speed[i].updated_at_lap && race_track.target_speed[i].max_angle < 45){
            race_track.target_speed[i].updated_at_lap = location_hist.buff[buff_ind_n].lap;
            race_track.target_speed[i].target_speed += 0.1;
            printf(" curve %d Max angle was %f increase target speed with 0.1\n",i,race_track.target_speed[i].max_angle);
          }

          if(location_hist.buff[buff_ind_n].lap != race_track.target_speed[i].updated_at_lap && race_track.target_speed[i].max_angle > 55){
            race_track.target_speed[i].updated_at_lap = location_hist.buff[buff_ind_n].lap;
            race_track.target_speed[i].target_speed -= 0.1;
          }

          printf("Target speed suoralla kurville %d indeksi %d\n", i , race_track.target_speed[i].piece_ind);
          target_speed = race_track.target_speed[i].target_speed;
          break;
        }
      i++;
      //at the end of the lap, use target speed from first corner
      if(i == race_track.target_cnt){
        target_speed = race_track.target_speed[0].target_speed;
      }
    }



	  ticks_to_event = (dist_to_event / speed) -2; // this is rough estimation and ticks_to curve is too bit too small, thus we use correction factor.

	  ticks_to_event = ticks_to_event < 0 ? 0:ticks_to_event;

	  acceleration = acceleration > 0 ? DEFAULT_BREAKING_ACC : acceleration;

	  printf("Break before curve throttle 0, dist_to_event %f ticks_to_event %f estimated speed %f estimated angle %f target_speed %f \n",
	      dist_to_event ,ticks_to_event +1, (speed + (acceleration*ticks_to_event)), (current_angle + (angle_acceleration*ticks_to_event)),target_speed);

	  if(speed == NOT_VALID ){
		  throttle = 1;
	  } else if(((speed + (acceleration*ticks_to_event)) > (target_speed+0.1) ) ||
			  (current_angle > 0  && ((current_angle + (angle_acceleration*ticks_to_event)) > MAX_INCOMING_ANGLE)) ||
			  (current_angle < 0  && ((current_angle + (angle_acceleration*ticks_to_event)) < -MAX_INCOMING_ANGLE))){

        throttle = 0.0;// Break before curve
	    //throttle = location_hist.buff[buff_ind_n_1].throttle - 0.4;
	  } else if (((speed + (acceleration*ticks_to_event)) < target_speed) && acceleration < 0.05  ){
	    throttle = location_hist.buff[buff_ind_n_1].throttle + (turbo_available.active ? 0.2 / turbo_available.turbo_factor : 0.2 );
	  } else if (((speed + (acceleration*ticks_to_event)) > target_speed) && acceleration > 0.05   ){
	        throttle = location_hist.buff[buff_ind_n_1].throttle - (turbo_available.active ? 0.2 / turbo_available.turbo_factor : 0.2 );
	  } else {
	    //printf("straight throttle 1.0\n");
	    //throttle = 1.0; // pedal to the metal as straight line ahead
	    throttle = location_hist.buff[buff_ind_n_1].throttle + (turbo_available.active ? 0.1 / turbo_available.turbo_factor : 0.1 );
	  }


	} else {
	  /* check curve speed */
	  radius = race_track.track_pieces[location_hist.buff[buff_ind_n].piece_ind].radius;
	  curve_angle = race_track.track_pieces[location_hist.buff[buff_ind_n].piece_ind].angle;

	  lane_delta = 0;
	  for(i = 0; i < race_track.lane_cnt; i++){
		  if(location_hist.buff[buff_ind_n].end_lane_index == race_track.lanes[i].lane_ind){
			  lane_delta = race_track.lanes[i].distance_from_cntr;
			  break;
		  }
	  }

	  if (curve_angle >= 0){
		  lane_delta = lane_delta * -1;
	  }

	  piece_length = (2 * PI * (radius + lane_delta)*curve_angle)/CIRCLE_DEG;
	  if(piece_length < 0){
		  piece_length = piece_length * -1;
	  }

	  target_speed = 30;
	  i = 0;
	  while (i < race_track.target_cnt){

	    if(i+1 < race_track.target_cnt){
	      if((location_hist.buff[buff_ind_n].piece_ind >= race_track.target_speed[i].piece_ind) && (location_hist.buff[buff_ind_n].piece_ind < race_track.target_speed[i+1].piece_ind)){
	        if(race_track.target_speed[i].max_angle < abs_car_angle){
	          race_track.target_speed[i].max_angle = abs_car_angle;
	        }
	        printf("Target speed kurvissa kurville %d indeksi %d\n", i , race_track.target_speed[i].piece_ind);
	        target_speed = race_track.target_speed[i].target_speed;
	        break;
	      }
	    } else {
        if((location_hist.buff[buff_ind_n].piece_ind >= race_track.target_speed[i].piece_ind)){
          if(race_track.target_speed[i].max_angle < abs_car_angle){
            race_track.target_speed[i].max_angle = abs_car_angle;
          }
          printf("Target speed kurvissa_ kurville %d indeksi %d\n", i , race_track.target_speed[i].piece_ind);
          target_speed = race_track.target_speed[i].target_speed;
          break;
        }
	    }
	    i++;
	  }

	  angle_delta = location_hist.buff[buff_ind_n].angle - location_hist.buff[buff_ind_n_1].angle;

	  printf("curve_angle %f angle_delta %f angle_delta / limit %f \n",curve_angle,angle_delta, (angle_delta / ANGLE_LIMIT));

	  //calculate how long it takes to curve if throttle is released now
	  dist_to_event = piece_length - in_piece_dist;
	  i = 1;
	  while (i < race_track.piece_cnt){

		  if(location_hist.buff[buff_ind_n].piece_ind + i >= race_track.piece_cnt){
			  piece = &race_track.track_pieces[(location_hist.buff[buff_ind_n].piece_ind + i)-race_track.piece_cnt];
		  } else {
			  piece = &race_track.track_pieces[location_hist.buff[buff_ind_n].piece_ind + i];
		  }

		  if(piece->shape == CURVE){
			  dist_to_event += piece->length;
			  tmp = (2 * PI * (radius + lane_delta)*curve_angle)/CIRCLE_DEG;
			  if(tmp < 0){
				  tmp = tmp * -1;
			  }
			  dist_to_event += tmp;
		  } else {
			  break;
		  }
		  i++;
	  }


	  ticks_to_event = (dist_to_event / speed) -1; // this is rough estimation and ticks_to curve is too bit too small, thus we use correction factor.

	  ticks_to_event = ticks_to_event < 0 ? 0:ticks_to_event;

	  if(abs_car_angle < 55  && ticks_to_event < 2 ){
	    //printf("curve ending throttle 1.0\n");
	    throttle = (turbo_available.active ? 1.0 / turbo_available.turbo_factor : 1.0 ); // pedal to the metal as straight line ahead

/*	  } else if(curve_angle > 0 && location_hist.buff[buff_ind_n].angle > 0 && (angle_delta /(ANGLE_LIMIT - location_hist.buff[buff_ind_n].angle)) > ANGLE_DELTA_PROS){
	    throttle = location_hist.buff[buff_ind_n_1].throttle - 0.3; // decrease throttle a little bit
	    printf("curve_0 decrease due angle delta throttle %f angle_delta / 60 = %f \n",throttle,(angle_delta / (ANGLE_LIMIT - location_hist.buff[buff_ind_n].angle)));

	  } else if (curve_angle < 0 && location_hist.buff[buff_ind_n].angle < 0  && (angle_delta / (ANGLE_LIMIT + location_hist.buff[buff_ind_n].angle)) < -ANGLE_DELTA_PROS) {
		    throttle = location_hist.buff[buff_ind_n_1].throttle - 0.3; // decrease throttle a little bit
		    printf("curve_1 decrease due angle delta throttle %f angle_delta / 60 = %f \n",throttle,(angle_delta / (ANGLE_LIMIT + location_hist.buff[buff_ind_n].angle)));
*/
	  } else if(( (curve_angle > 0 && current_angle < 56 && angle_acceleration <0.1) || (curve_angle > 0 &&current_angle > -56 && angle_acceleration < -0.1))){
		  throttle = location_hist.buff[buff_ind_n_1].throttle+ (turbo_available.active ? 0.1 / turbo_available.turbo_factor : 0.1 );
	  } else if(( (current_angle > 20 && angle_acceleration > 2) || (current_angle < -20 && angle_acceleration < -2))){
		  throttle = location_hist.buff[buff_ind_n_1].throttle - (turbo_available.active ? 0.4 / turbo_available.turbo_factor : 0.4 ); // decrease throttle a little bit
	  } else if(abs_car_angle > 50 /*&& ( (curve_angle > 0 && angle_acceleration > 0.05) || (curve_angle < 0 && angle_acceleration < -0.05))*/) {
	    throttle = location_hist.buff[buff_ind_n_1].throttle - (turbo_available.active ? 0.2 / turbo_available.turbo_factor : 0.2 ); // decrease throttle a little bit
	    //printf("curve decrease throttle %f\n",throttle);

		  throttle = location_hist.buff[buff_ind_n_1].throttle - (turbo_available.active ? 0.4 / turbo_available.turbo_factor : 0.4 );
	 /* } else if(speed > (target_speed + 0.1)){
	    throttle = location_hist.buff[buff_ind_n_1].throttle - (turbo_available.active ? 0.15 / turbo_available.turbo_factor : 0.15 );
	  } else if(speed < target_speed){
	    throttle = location_hist.buff[buff_ind_n_1].throttle + (turbo_available.active ? 0.15 / turbo_available.turbo_factor : 0.15 );
*/

	  } else {
	    throttle = location_hist.buff[buff_ind_n_1].throttle + (turbo_available.active ? 0.15 / turbo_available.turbo_factor : 0.15 ); // increace throttle a little bit
	    //printf("curve increase throttle %f\n",throttle);
	  }

	}


	if(throttle < 0){
	  //printf("Throttle limited to 0, it was %f\n", throttle);
	  throttle = 0;
	} else if (throttle > 1.0){
	  //printf("Throttle limited to 1, it was %f\n", throttle);
	  throttle = 1.0;
	}

	location_hist.buff[location_hist.ind].throttle = throttle;

	return throttle;
}

void print_location_and_car_data(){

#if PRINT_LOCATION_UPDATE

	 printf("current piece type %d length %f radius %f angle %f\n",
	     race_track.track_pieces[location_hist.buff[location_hist.ind].piece_ind].shape,
	     race_track.track_pieces[location_hist.buff[location_hist.ind].piece_ind].length,
	     race_track.track_pieces[location_hist.buff[location_hist.ind].piece_ind].radius,
	     race_track.track_pieces[location_hist.buff[location_hist.ind].piece_ind].angle);

    printf("game_tick %d end_lane %d lap %d piece_ind %d in_piece_dist %f speed %f acceleration %f angle %f angle_acceleration %f throttle %f turbo %d \n",
        location_hist.buff[location_hist.ind].game_tick,
        location_hist.buff[location_hist.ind].end_lane_index,
        location_hist.buff[location_hist.ind].lap,
        location_hist.buff[location_hist.ind].piece_ind,
        location_hist.buff[location_hist.ind].in_piece_dist,
        location_hist.buff[location_hist.ind].speed,
        location_hist.buff[location_hist.ind].acceleration,
        location_hist.buff[location_hist.ind].angle,
        location_hist.buff[location_hist.ind].angle_acceleration,
        location_hist.buff[location_hist.ind].throttle,
        turbo_available.active);
#endif

  return;
}

double do_location_update_and_calculte_new_throttle(cJSON* json){
#define STATIC_THROTTLE 1
  double throttle;

  if(update_location_info(find_item("data", json))){

    if(calculate_speedometer_info()){
      //new location info received, use that to calculate new throttle
      throttle = calculate_throttle();
    }else {
      ////take last valid if available
      printf("Use static throttle as speedometer info is not present\n");
      throttle = location_hist.buff[location_hist.ind].throttle = STATIC_THROTTLE;
    }
    print_location_and_car_data();



    update_ring_buff_ind();

  } else {
    //as a last option use static throttle
    printf("Use static throttle\n");
    throttle = STATIC_THROTTLE;
  }

  return throttle;
}

static cJSON* switch_lane_msg()
{
  cJSON* msg = NULL;
  int buff_ind_n_1 = get_ring_buff_ind(-1);
  int piece_ind;
  int i;
  int switch_count = 0;
  double total_angle;
  track_piece_t* piece;

  if(lane_switch_info.last_checked_piece != location_hist.buff[buff_ind_n_1].piece_ind ){

    lane_switch_info.last_checked_piece = location_hist.buff[buff_ind_n_1].piece_ind;

    //printf("check switch piece %d\n", lane_switch_info.last_checked_piece);

    i = 0;
    piece_ind = location_hist.buff[buff_ind_n_1].piece_ind;
    while( i < race_track.piece_cnt){
      if(piece_ind + i >= race_track.piece_cnt){
        piece = &race_track.track_pieces[(piece_ind + i)-race_track.piece_cnt];
      } else {
        piece = &race_track.track_pieces[piece_ind + i];
      }
      if(piece->switchable){
        piece_ind = (piece_ind + i) >= race_track.piece_cnt ? ((piece_ind + i)-race_track.piece_cnt) : (piece_ind + i);
        break;
      }
      i++;
    }
    //printf("laskekulma palasta %d\n",piece_ind);

    i = 0;
    while( i < race_track.piece_cnt){
      if(piece_ind + i >= race_track.piece_cnt){
        piece = &race_track.track_pieces[(piece_ind + i)-race_track.piece_cnt];
      } else {
        piece = &race_track.track_pieces[piece_ind + i];
      }

      total_angle += piece->angle;

      if(piece->switchable){
        switch_count++;
      }

      if(switch_count == race_track.lane_cnt){
        //printf("lopetti anglen laskun palaan %d\n",piece_ind + i >= race_track.piece_cnt ? (piece_ind + i)-race_track.piece_cnt : piece_ind + i);
        break;
      }

      i++;
    }



    if(total_angle > 0){
      printf("total angle %f vaihda vasen\n",total_angle);
      msg = make_msg("switchLane", cJSON_CreateString("Left"));
    } else if(total_angle < 0){
      printf("total angle %f vaihda oikea\n",total_angle);
      msg = make_msg("switchLane", cJSON_CreateString("Right"));
    } else {
      //printf("total angle %f ala vaihda\n",total_angle);
    }



  }

  return msg;
}

cJSON* enable_turbo_msg(){

  cJSON* msg = NULL;

  int buff_ind_n_1 = get_ring_buff_ind(-1);
  piece_shape_t piece_shape;

  if(turbo_available.available == 1){
    if(race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind].shape == STRAIGTH){
      if(location_hist.buff[buff_ind_n_1].piece_ind + 1 >= race_track.piece_cnt){
        piece_shape = race_track.track_pieces[0].shape;
      } else {
        piece_shape = race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind + 1].shape;
      }
      if (piece_shape == STRAIGTH){
        if(location_hist.buff[buff_ind_n_1].piece_ind + 2 >= race_track.piece_cnt){
          piece_shape = race_track.track_pieces[race_track.piece_cnt - location_hist.buff[buff_ind_n_1].piece_ind + 2].shape;
        } else {
          piece_shape = race_track.track_pieces[location_hist.buff[buff_ind_n_1].piece_ind + 2].shape;
        }

#if 1
        if (piece_shape == STRAIGTH){
          printf("turbo enabled\n");
          msg = make_msg("turbo", cJSON_CreateString("Pedal to the metal"));
          turbo_available.available = 0;
          turbo_available.active = 1;
          turbo_available.enabled_at_tick = location_hist.buff[buff_ind_n_1].game_tick;
        }
#endif
      }
    }
  } else {
	  if(location_hist.buff[buff_ind_n_1].game_tick - turbo_available.enabled_at_tick > turbo_available.duration){
		  turbo_available.active = 0;
	  }
  }


  return msg;
}

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;
    int i;
    double throttle;

    /* Juho Inits */
    location_hist.ind = 0;
    for(i = 0; i < LOCATION_RINGBUFFER_SIZE; i++){
    	location_hist.buff[i].valid = 0;
    }

#if 0


    json = cJSON_Parse(test_message);
    store_race_track(find_item("track", json));
    cJSON_Delete(json);

    my_car.name = test_car_name;
    printf("oman auton nimi %s \n", my_car.name);
    json = cJSON_Parse(test_message_positions);
    do_location_update_and_calculte_new_throttle(json);
    do_location_update_and_calculte_new_throttle(json);
    do_location_update_and_calculte_new_throttle(json);
    do_location_update_and_calculte_new_throttle(json);

    cJSON_Delete(json);
    return 0;
#endif

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);


    //juho temp hack
#if 0
    json = join_msg(argv[3], argv[4]);
#else
    json = join_msg(argv[3], "wb0Ghpu6BF6BOQ");
#endif
    write_msg(sock, json);
    cJSON_Delete(json);

    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name)) {
          throttle = do_location_update_and_calculte_new_throttle(json);
#if 1
          if (location_hist.buff[get_ring_buff_ind(-1)].game_tick > 3){
        	  if((msg = enable_turbo_msg()) == NULL){
        		  if((msg = switch_lane_msg()) == NULL){
        			  msg = throttle_msg(throttle);
        		  }
        	  }
          } else {
        	  msg = throttle_msg(throttle);
          }
#else
          msg = throttle_msg(throttle);
#endif
        } else if (!strcmp("yourCar", msg_type_name)) {
        	store_car_info(find_item("data", json));
        	printf("auton nimi %s \n", my_car.name);
          msg = ping_msg();
        } else if (!strcmp("gameInit", msg_type_name)) {
        	store_race_track(find_item("track", json));
        	 msg = ping_msg();
        } else if (!strcmp("turboAvailable", msg_type_name)) {
          store_turbo_available(find_item("data", json));
          msg = ping_msg();
        } else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        write_msg(sock, msg);

        printf("\n"); //add couple of empty lines between ticks

        cJSON_Delete(msg);
        cJSON_Delete(json);

    }
    /*juho added*/
    if(race_track.track_pieces != NULL){
      free(race_track.track_pieces);
    }
    if(race_track.lanes != NULL){
      free(race_track.lanes);
    }
    if(race_track.target_speed != NULL){
      free(race_track.target_speed);
    }
    if(my_car.name != NULL){
      free(my_car.name);
    }

    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
    	//printf("%c", *readp);
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);
    //printf("write message %s\n", msg_str);
    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
